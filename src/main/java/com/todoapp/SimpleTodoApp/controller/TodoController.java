package com.todoapp.SimpleTodoApp.controller;

import com.todoapp.SimpleTodoApp.model.TodoItem;
import com.todoapp.SimpleTodoApp.repo.TodoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/todo")
public class TodoController {

    @Autowired
    private TodoRepo todoRepo;

    @GetMapping("/")
    public String home(@AuthenticationPrincipal OidcUser user) {
        return "Welcome, "+ user.getFullName() + "!";
    }

    @GetMapping
    public List<TodoItem> findAll(){
        return todoRepo.findAll();
    }

    @GetMapping({"/{id}"})
    public TodoItem getTodo(@PathVariable Long id){
        return todoRepo.findById(id).get();
    }

    @PostMapping
    public TodoItem save(@RequestBody TodoItem todoItem){
        return todoRepo.save(todoItem);
    }
    @PutMapping
    public TodoItem update(@RequestBody TodoItem todoItem){
        return  todoRepo.save(todoItem);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id){
        todoRepo.deleteById(id);
    }


}
